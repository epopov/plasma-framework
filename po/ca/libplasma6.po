# Translation of libplasma6.po to Catalan
# Copyright (C) 2014-2024 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# SPDX-FileCopyrightText: 2014, 2015, 2016, 2018, 2019, 2020, 2021, 2022, 2023, 2024 Josep M. Ferrer <txemaq@gmail.com>
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2014, 2018, 2019, 2020, 2022.
# Empar Montoro Martín <montoro_mde@gva.es>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: libplasma\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-09-23 00:40+0000\n"
"PO-Revision-Date: 2024-08-23 11:00+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 24.05.2\n"

#: declarativeimports/plasmaextracomponents/qml/BasicPlasmoidHeading.qml:81
#, kde-format
msgid "More actions"
msgstr "Més accions"

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:524
#, kde-format
msgctxt "@action:button"
msgid "Collapse"
msgstr "Redueix"

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:524
#, kde-format
msgctxt "@action:button"
msgid "Expand"
msgstr "Expandeix"

#: declarativeimports/plasmaextracomponents/qml/PasswordField.qml:49
#, kde-format
msgid "Password"
msgstr "Contrasenya"

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:60
#, kde-format
msgid "Search…"
msgstr "Cerca…"

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:62
#, kde-format
msgid "Search"
msgstr "Cerca"

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:73
#, kde-format
msgid "Clear search"
msgstr "Neteja la cerca"

#: plasma/applet.cpp:293
#, kde-format
msgid "Unknown"
msgstr "Desconegut"

#: plasma/applet.cpp:736
#, kde-format
msgid "Activate %1 Widget"
msgstr "Activa el giny %1"

#: plasma/containment.cpp:97 plasma/private/applet_p.cpp:101
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Remove %1"
msgstr "Elimina %1"

#: plasma/containment.cpp:103 plasma/corona.cpp:360 plasma/corona.cpp:481
#, kde-format
msgid "Enter Edit Mode"
msgstr "Entra en el mode d'edició"

#: plasma/containment.cpp:106 plasma/private/applet_p.cpp:106
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Configure %1..."
msgstr "Configura %1..."

#: plasma/corona.cpp:309 plasma/corona.cpp:466
#, kde-format
msgid "Lock Widgets"
msgstr "Bloqueja els ginys"

#: plasma/corona.cpp:309
#, kde-format
msgid "Unlock Widgets"
msgstr "Desbloqueja els ginys"

#: plasma/corona.cpp:358
#, kde-format
msgid "Exit Edit Mode"
msgstr "Surt del mode d'edició"

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:9
#, kde-format
msgid "Whether or not to create an on-disk cache for the theme."
msgstr "Determina si s'ha de crear o no una memòria cau al disc per al tema."

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:14
#, kde-format
msgid ""
"The maximum size of the on-disk Theme cache in kilobytes. Note that these "
"files are sparse files, so the maximum size may not be used. Setting a "
"larger size is therefore often quite safe."
msgstr ""
"La mida màxima de la memòria cau al disc per al tema en kilobytes. Cal tenir "
"en compte que aquests fitxers són fitxers dispersos, per tant, gairebé no "
"s'usarà la mida màxima. Per consegüent, es força segur definir una mida gran."

#: plasma/private/applet_p.cpp:119
#, kde-format
msgid "Show Alternatives..."
msgstr "Mostra les alternatives..."

#: plasma/private/applet_p.cpp:232
#, kde-format
msgid "Widget Removed"
msgstr "S'ha eliminat el giny"

#: plasma/private/applet_p.cpp:233
#, kde-format
msgid "The widget \"%1\" has been removed."
msgstr "S'ha eliminat el giny «%1»."

#: plasma/private/applet_p.cpp:237
#, kde-format
msgid "Panel Removed"
msgstr "S'ha eliminat el plafó"

#: plasma/private/applet_p.cpp:238
#, kde-format
msgid "A panel has been removed."
msgstr "S'ha eliminat un plafó."

#: plasma/private/applet_p.cpp:241
#, kde-format
msgid "Desktop Removed"
msgstr "S'ha eliminat l'escriptori"

#: plasma/private/applet_p.cpp:242
#, kde-format
msgid "A desktop has been removed."
msgstr "S'ha eliminat un escriptori."

#: plasma/private/applet_p.cpp:245
#, kde-format
msgid "Undo"
msgstr "Desfés"

#: plasma/private/applet_p.cpp:336
#, kde-format
msgid "Widget Settings"
msgstr "Configuració del giny"

#: plasma/private/applet_p.cpp:343
#, kde-format
msgid "Remove this Widget"
msgstr "Suprimeix aquest giny"

#: plasma/private/containment_p.cpp:58
#, kde-format
msgid "Remove this Panel"
msgstr "Elimina aquest plafó"

#: plasma/private/containment_p.cpp:60
#, kde-format
msgid "Remove this Activity"
msgstr "Elimina aquesta activitat"

#: plasma/private/containment_p.cpp:66
#, kde-format
msgid "Activity Settings"
msgstr "Configuració de l'activitat"

#: plasma/private/containment_p.cpp:78
#, kde-format
msgid "Add or Manage Widgets…"
msgstr "Afegeix o gestiona ginys…"

#: plasma/private/containment_p.cpp:197
#, kde-format
msgid "Could not find requested component: %1"
msgstr "No s'ha pogut trobar el component sol·licitat: %1"

#: plasmaquick/appletquickitem.cpp:544
#, kde-format
msgid "The root item of %1 must be of type ContainmentItem"
msgstr "L'element arrel de %1 cal que sigui del tipus ContainmentItem"

#: plasmaquick/appletquickitem.cpp:549
#, kde-format
msgid "The root item of %1 must be of type PlasmoidItem"
msgstr "L'element arrel de %1 cal que sigui del tipus PlasmoidItem"

#: plasmaquick/appletquickitem.cpp:557
#, kde-format
msgid "Unknown Applet"
msgstr "Miniaplicació desconeguda"

#: plasmaquick/appletquickitem.cpp:571
#, kde-format
msgid ""
"This Widget was written for an unknown older version of Plasma and is not "
"compatible with Plasma %1. Please contact the widget's author for an updated "
"version."
msgstr ""
"Aquest giny s'ha escrit per a una versió antiga desconeguda del Plasma i no "
"és compatible amb el Plasma %1. Poseu-vos en contacte amb l'autor del giny "
"per a una versió actualitzada."

#: plasmaquick/appletquickitem.cpp:574 plasmaquick/appletquickitem.cpp:581
#: plasmaquick/appletquickitem.cpp:587
#, kde-format
msgid "%1 is not compatible with Plasma %2"
msgstr "%1 no és compatible amb el Plasma %2"

#: plasmaquick/appletquickitem.cpp:578
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please contact the widget's author for an updated version."
msgstr ""
"Aquest giny s'ha escrit per al Plasma %1 i no és compatible amb el Plasma "
"%2. Poseu-vos en contacte amb l'autor del giny per a una versió actualitzada."

#: plasmaquick/appletquickitem.cpp:584
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please update Plasma in order to use the widget."
msgstr ""
"Aquest giny s'ha escrit per al Plasma %1 i no és compatible amb el Plasma "
"%2. Actualitzeu el Plasma per tal de fer servir el giny."

#: plasmaquick/appletquickitem.cpp:593 plasmaquick/appletquickitem.cpp:615
#, kde-format
msgid "Sorry! There was an error loading %1."
msgstr "S'ha produït un error en carregar %1."

#: plasmaquick/appletquickitem.cpp:610
#, kde-format
msgid "Error loading QML file: %1 %2"
msgstr "Error en carregar el fitxer QML: %1 %2"

#: plasmaquick/appletquickitem.cpp:613
#, kde-format
msgid "Error loading Applet: package %1 does not exist."
msgstr "Error en carregar la miniaplicació: el paquet %1 no existeix."

#: plasmaquick/configview.cpp:231
#, kde-format
msgid "%1 — %2 Settings"
msgstr "%1 — Configuració de %2"

#: plasmaquick/configview.cpp:232
#, kde-format
msgid "%1 Settings"
msgstr "Configuració de %1"

#: plasmaquick/plasmoid/containmentitem.cpp:569
#, kde-format
msgid "Plasma Package"
msgstr "Paquet del Plasma"

#: plasmaquick/plasmoid/containmentitem.cpp:573
#, kde-format
msgid "Install"
msgstr "Instal·la"

#: plasmaquick/plasmoid/containmentitem.cpp:584
#, kde-format
msgid "Package Installation Failed"
msgstr "La instal·lació del paquet ha fallat"

#: plasmaquick/plasmoid/containmentitem.cpp:600
#, kde-format
msgid "The package you just dropped is invalid."
msgstr "El paquet que acabeu de deixar no és vàlid."

#: plasmaquick/plasmoid/containmentitem.cpp:609
#, kde-format
msgid "Widgets"
msgstr "Ginys"

#: plasmaquick/plasmoid/containmentitem.cpp:614
#, kde-format
msgctxt "Add widget"
msgid "Add %1"
msgstr "Afegeix el %1"

#: plasmaquick/plasmoid/containmentitem.cpp:628
#, kde-format
msgctxt "Add icon widget"
msgid "Add Icon"
msgstr "Afegeix una icona"

#: plasmaquick/plasmoid/containmentitem.cpp:640
#, kde-format
msgid "Wallpaper"
msgstr "Fons de pantalla"

#: plasmaquick/plasmoid/containmentitem.cpp:650
#, kde-format
msgctxt "Set wallpaper"
msgid "Set %1"
msgstr "Estableix el %1"

#: plasmaquick/plasmoid/dropmenu.cpp:29
#, kde-format
msgid "Content dropped"
msgstr "Contingut descartat"

#~ msgid "Add Widgets..."
#~ msgstr "Afegeix ginys..."

#~ msgctxt "Package file, name of the widget"
#~ msgid "Could not open the %1 package required for the %2 widget."
#~ msgstr "No s'ha pogut obrir el paquet %1 requerit pel giny %2."

#~ msgid ""
#~ "Sharing a widget on the network allows you to access this widget from "
#~ "another computer as a remote control."
#~ msgstr ""
#~ "La compartició d'un giny en la xarxa permet accedir-hi des de qualsevol "
#~ "altre ordinador, com si es tractés d'un control remot."

#~ msgid "Share this widget on the network"
#~ msgstr "Comparteix aquest giny per la xarxa"

#~ msgid "Allow everybody to freely access this widget"
#~ msgstr "Permet a tothom l'accés lliure a aquest giny"

#~ msgctxt "Error message, tried to start an invalid service"
#~ msgid "Invalid (null) service, can not perform any operations."
#~ msgstr "Servei no vàlid (nul), no pot executar cap operació."
